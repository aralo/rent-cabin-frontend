import { Component, Input } from '@angular/core';

@Component({
  selector: 'cabin-card',
  templateUrl: './cabin-card.component.html',
  styleUrls: ['./cabin-card.component.scss']
})
export class CabinCardComponent {
  @Input() name = '';
  @Input() imageUrl = '';
  @Input() description = '';
}
