import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { CabinRepositoryService } from 'src/app/repository/cabin-repository.service';
import { Cabin } from 'src/domain/cabin';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent {
  cabins$: Observable<Array<Cabin>> = of([]);

  constructor(private cabinRepository: CabinRepositoryService) {
    this.cabins$ = this.cabinRepository.getAllCabins();
  }
}
