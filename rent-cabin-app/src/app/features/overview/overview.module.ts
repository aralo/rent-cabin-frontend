import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverviewComponent } from './overview.component';
import { CabinCardComponent } from './cabin-card/cabin-card.component';
import { AutoFitPipe } from 'src/app/toolbox/pipes/auto-fit.pipe';
import { RepositoryModule } from 'src/app/repository/repository.module';


@NgModule({
  declarations: [
    OverviewComponent,
    CabinCardComponent,
    AutoFitPipe
  ],
  imports: [
    CommonModule,
    RepositoryModule
  ],
  exports: [OverviewComponent, CabinCardComponent]
})
export class OverviewModule { }
