import { createAction, props } from "@ngrx/store";

/*
    As a: user,
    I want: to login
    so that: I can review my reservations
*/

//[Origin] Descriptive name.
export const login = createAction(
    '[UserLogin] Login',
    props<{ username: string; password: string }>()
);