import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { UserLogin } from "./user-login.model";

@Injectable({ providedIn: 'root' })

export class UserLoginService {
    constructor(private store: Store<UserLogin>) {

    }

}

