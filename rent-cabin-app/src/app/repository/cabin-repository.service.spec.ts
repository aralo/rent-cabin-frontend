import { TestBed } from '@angular/core/testing';

import { CabinRepositoryService } from './cabin-repository.service';

describe('CabinRepositoryService', () => {
  let service: CabinRepositoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CabinRepositoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
