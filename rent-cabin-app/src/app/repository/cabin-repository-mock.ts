import { Cabin } from "src/domain/cabin"

export const mockCabins: Array<Cabin> = [
    {
        name: 'Forest Cabin',
        imageUrl: '../../../assets/images/cabin-demo.png',
        description: 'Lorem ipsum dolor sit amet consectetur adipiscing elit, mauris litora pretium sollicitudin quis orci euismod congue, torquent duis phasellus sociis vehicula montes. Aptent diam commodo tortor condimentum enim molestie eleifend gravida interdum, ligula posuere et aliquet scelerisque tempus leo dis pellentesque porttitor, facilisi praesent egestas tellus platea pulvinar massa neque.'
    },
    {
        name: 'River Cabin',
        imageUrl: '../../../assets/images/cabin-demo.png',
        description: 'Lorem ipsum dolor sit amet consectetur adipiscing elit cursus sodales odio, quam arcu vel augue integer fames blandit non eros ut, iaculis elementum euismod a curae vivamus fermentum sociosqu lacus.'
    },
    {
        name: 'Suny Cabin',
        imageUrl: '../../../assets/images/cabin-demo.png',
        description: 'Risus cras dictumst cum luctus magnis id aliquam varius, augue arcu iaculis curabitur pharetra lacus nisl duis vel, morbi porta convallis quis ridiculus sodales est.'
    },
    {
        name: 'Tree Cabin',
        imageUrl: '../../../assets/images/cabin-demo.png',
        description: 'Ligula tellus sociis varius hendrerit ac vel litora egestas augue, natoque ullamcorper nulla tempor sem aliquam mus nam tincidunt'
    }
]