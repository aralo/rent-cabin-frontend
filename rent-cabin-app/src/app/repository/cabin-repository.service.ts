import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Cabin } from 'src/domain/cabin';
import { mockCabins } from './cabin-repository-mock';
import { RepositoryModule } from './repository.module';

@Injectable({
  providedIn: RepositoryModule
})
export class CabinRepositoryService {

  constructor(private _http: HttpClient) { }

  public getAllCabins(): Observable<Cabin[]> {
    //return of(mockCabins);
    const url = "https://rent-cabin-api.herokuapp.com/api/1.0/cabin";
    return this._http.get<Cabin[]>(url);
  }
}
