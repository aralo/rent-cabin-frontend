export interface Cabin {
    name: string;
    imageUrl: string;
    description: string;
}